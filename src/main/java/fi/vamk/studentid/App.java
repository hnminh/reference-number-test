package fi.vamk.studentid;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    public static boolean validateReferenceNumberLength(String ref){
        Pattern pattern = Pattern.compile("[0-9]");
        Matcher matcher = pattern.matcher(ref);
        long result = matcher.results().count();
        if (result > 3 && result < 20)
            return true;
        else 
            return false;
    }

    public static boolean validateReferenceNumberGroups(String ref){
        String[] groups = ref.split(" ");
        if (groups[0].length() > 5)
            return false;
        
        for (int i = 1; i < groups.length; i++)
            if (groups[i].length() != 5)
                return false;
        
        return true;
    }

    public static boolean validateReferenceNumberCharacters(String ref){
        Pattern pattern = Pattern.compile("[^0-9 ]");
        Matcher matcher = pattern.matcher(ref);
        long result = matcher.results().count();
        if (result > 0)
            return false;
        else 
            return true;
    }

    public static boolean validateReferenceNumberCheckingNumber(String ref){
        if (validateReferenceNumberLength(ref) && validateReferenceNumberGroups(ref) && validateReferenceNumberCharacters(ref)){
            ref = ref.replaceAll(" ", "");
            String[] digits = ref.split("");

            int checkingNumber = Integer.parseInt(digits[digits.length - 1]);
            int total = 0;
            int count = 0;
            int coefficient = 0;
            for (int i = digits.length - 2; i >= 0; i--){
                count++;
                if (count%3 == 1)
                    coefficient = 7;
                else if (count%3 == 2)
                    coefficient = 3;
                else 
                    coefficient = 1;
                
                total += coefficient*Integer.parseInt(digits[i]);
            }

            int result = 10 - total%10;
            if (result == checkingNumber)
                return true;
            else
                return false;
        }
        else 
            return false;
    }
}
