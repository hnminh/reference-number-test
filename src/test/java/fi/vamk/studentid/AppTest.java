package fi.vamk.studentid;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */

     
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void testReferenceNumberMinLength(){
        assertTrue(App.validateReferenceNumberLength("1232"));
        assertTrue(App.validateReferenceNumberLength("12321"));
        assertFalse(App.validateReferenceNumberLength("123"));
        assertFalse(App.validateReferenceNumberLength("1"));
    }

    @Test
    public void testReferenceNumberMaxLength(){
        assertTrue(App.validateReferenceNumberLength("1234567890123456787"));
        assertTrue(App.validateReferenceNumberLength("12 34567 89012 34567"));
        assertFalse(App.validateReferenceNumberLength("1 23456 78901 23456 78901"));
        assertFalse(App.validateReferenceNumberLength("12345678901234567890"));
    }

    @Test
    public void testReferenceNumberGrouping(){
        assertTrue(App.validateReferenceNumberGroups("1234"));
        assertTrue(App.validateReferenceNumberGroups("12345"));
        assertTrue(App.validateReferenceNumberGroups("11 12345"));
        assertFalse(App.validateReferenceNumberGroups("12345 11"));
        assertFalse(App.validateReferenceNumberGroups("1234  12345"));
        assertFalse(App.validateReferenceNumberGroups("12 234"));
        assertFalse(App.validateReferenceNumberGroups("12345678901234567890"));
    }

    @Test
    public void testReferenceNumberCharacters(){
        assertTrue(App.validateReferenceNumberCharacters("1232"));
        assertTrue(App.validateReferenceNumberCharacters("12322 22133"));
        assertTrue(App.validateReferenceNumberCharacters("12 12332"));
        assertFalse(App.validateReferenceNumberCharacters("13x323 23"));
        assertFalse(App.validateReferenceNumberCharacters("12  32@23"));
        assertFalse(App.validateReferenceNumberCharacters("12 # 32"));
    }

    @Test
    public void testReferenceNumberCheckingNumber(){
        assertTrue(App.validateReferenceNumberCheckingNumber("1232"));
        assertFalse(App.validateReferenceNumberCheckingNumber("12345 12345"));
    }
}
